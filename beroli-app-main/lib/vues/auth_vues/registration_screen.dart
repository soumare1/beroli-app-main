// ignore_for_file: deprecated_member_use

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import '../home_screen.dart';

class RegistartionScreen extends StatefulWidget {
  RegistartionScreen({Key? key}) : super(key: key);

  @override
  State<RegistartionScreen> createState() => _RegistartionScreenState();
}

class _RegistartionScreenState extends State<RegistartionScreen> {
  //final usersData = UsersData();

  // our form key
  final _formKey = GlobalKey<FormState>();
  // editing controller
  // Nom de Entreprise
  final nomEntrepriseEditingController = new TextEditingController();
  //Email
  final emailEditingController = new TextEditingController();
  //siret
  final siretEditingController = new TextEditingController();
  //mots de passe
  final passwordEditingController = new TextEditingController();
  //confirmation de mots de passe
  final confirmationPasswordEditingController = new TextEditingController();

//DBB
  void UserInfo(
      String uid,
      String nomEntrepriseEditingController,
      String emailEditingController,
      String siretEditingController,
      String passwordEditingController) async {
    DatabaseReference ref = FirebaseDatabase.instance.ref("users");

    await ref.set({
      "nomEntreprise": nomEntrepriseEditingController,
      "email": emailEditingController,
      "siret": siretEditingController,
      "password": passwordEditingController,
    });
  }

  @override
  Widget build(BuildContext context) {
    //Nom de Entreprise field
    final NomEntrepriseField = TextFormField(
      autofocus: false,
      controller: nomEntrepriseEditingController,
      keyboardType: TextInputType.text,
      //validator: () {},
      onSaved: (value) {
        nomEntrepriseEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.account_circle),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Nom de votre entreprise",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );
    //email field

    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailEditingController,
      keyboardType: TextInputType.emailAddress,
      //validator: () {},
      onSaved: (value) {
        emailEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.mail),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Email",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );
    //email field

    //siret field
    final siretField = TextFormField(
      autofocus: false,
      controller: siretEditingController,
      keyboardType: TextInputType.emailAddress,
      //validator: () {},
      onSaved: (value) {
        siretEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.account_balance_sharp),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "numero de Siret",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );
    //email field

    //passwordfield
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordEditingController,
      keyboardType: TextInputType.emailAddress,
      //validator: () {},
      onSaved: (value) {
        passwordEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "mots de passe",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );
    //email field

    //password Confirmation field
    final confirmationPasswordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: confirmationPasswordEditingController,
      keyboardType: TextInputType.emailAddress,
      //validator: () {},
      onSaved: (value) {
        confirmationPasswordEditingController.text = value!;
      },
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Confirmation de votre mots de passe",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );

    final signUpButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      //TO DO
      // color should change
      color: Color(0xFF146356),
      //
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: () {
          FirebaseAuth.instance
              .createUserWithEmailAndPassword(
                  email: emailEditingController.text,
                  password: passwordEditingController.text)
              .then((value) {
            FirebaseFirestore.instance
                .collection('ulisateur')
                .doc(value.user?.uid)
                .set({
              "nomEntreprise": nomEntrepriseEditingController.text,
              "email": emailEditingController.text,
              "siret": siretEditingController.text,
              "password": passwordEditingController.text,
            });
            print("Created New Account");
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => HomeScreen()));
          }).onError((error, stackTrace) {
            print("Error ${error.toString()}");
          });
        },
        child: Text("Sign Up",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
    );
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text('Inscription '),
        ),
        body: Center(
          child: SingleChildScrollView(
              child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                          height: 100,
                          child: Image.asset(
                            "assets/images/Beroli_logo.jpg",
                            fit: BoxFit.contain,
                          )),
                      SizedBox(height: 45),
                      NomEntrepriseField,
                      SizedBox(height: 20),
                      emailField,
                      SizedBox(height: 20),
                      siretField,
                      SizedBox(height: 20),
                      passwordField,
                      SizedBox(height: 20),
                      confirmationPasswordField,
                      SizedBox(height: 20),
                      signUpButton
                    ],
                  )),
            ),
          )),
        ));
  }
}
