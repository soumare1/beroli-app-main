import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/vues/home_screen.dart';
import 'package:flutter_application_1/vues/auth_vues/registration_screen.dart';
/*import 'package:fluttertoast/fluttertoast.dart';*/
import 'package:flutter_application_1/utils/createMaterialColor.dart';
import 'package:flutter_application_1/vues/vues_pages/Home.dart';

import '../vues_pages/Arborescence.dart';
//import 'package:flutter_application_1/vues/vues_pages/indexVue.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  void signIn(BuildContext context) async {
    try {
      final credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);
      print(credential);
    } on FirebaseAuthException catch (error) {
      var message = "Please Check Your Internet Connection ";
      if (error.message != null) {
        message = error.message!;
      }
    }
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        print(user.uid);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: ((context) => Application_vues()),
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      //on recuper la valeur
      onSaved: (email) {
        emailController.text;
        ;
      },

      validator: (email) {
        if (email!.isEmpty) {
          return ("Veillez entre votre adresse mail");
        }
        // Reg experssion for email validation
        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9+_.-]+.[a-z]")
            .hasMatch(email)) {
          return ("Veillez saisir un email valide");
        }
        // si le champ est vide retourne null
        return null;
      },

      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.mail),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Email",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );

    //password field
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordController,
      onSaved: (password) {
        passwordController.text;
      },
      validator: (password) {
        RegExp regex = new RegExp(r'^.{6,}$');
        if (password!.isEmpty) {
          return ("Le mot de passe est nécessaire pour se connecter");
        }
        if (!regex.hasMatch(password)) {
          return ("veuillez entrer un mot de passe valide (min. 6 caractères)");
        } else if (password.length < 8 || password.length > 15)
          return "La longueur du mot de passe est faible";
        return null;
      },
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );

    final loginButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      //TO DO
      // color should change
      color: Color(0xFF146356),
      //
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: () {
          signIn(context);
        },
        child: Text("Connexion",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
    );
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Connexion Page'),
        ),
        body: Center(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
                child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                            height: 200,
                            child: Image.asset(
                              "assets/images/Beroli_logo.jpg",
                              fit: BoxFit.contain,
                            )),
                        SizedBox(height: 45),
                        emailField,
                        SizedBox(height: 25),
                        passwordField,
                        SizedBox(height: 35),
                        loginButton,
                        SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Don't have a account ? "),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: ((context) =>
                                            RegistartionScreen())));
                              },
                              child: Text(
                                "Inscription",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15,
                                    color: Color(0xFF146356)),
                              ),
                            )
                          ],
                        )
                      ],
                    )),
              ),
            )),
          ),
        ));
  }
}
