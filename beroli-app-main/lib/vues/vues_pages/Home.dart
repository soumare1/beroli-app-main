import 'package:flutter/material.dart';

class Menu extends StatefulWidget implements PreferredSizeWidget {
  @override
  State<Menu> createState() => _MenuState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}

class _MenuState extends State<Menu> {
  //{String name, double price, String image}
  //final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    const Color vertF = Color(0xFF146356);
    const Color blanc = Color(0xFFFFFFFF);

    return Scaffold(
        body: Container(
      height: double.infinity,
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      child: Column(children: <Widget>[
        Container(
          height: 150,
          color: Colors.blue,
          child: Image.asset(
            'assets/images/Beroli_logo.jpg',
            fit: BoxFit.contain,
          ),
          width: double.infinity,
        ),
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text("noveau produit"),
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.greenAccent)),
                    onPressed: () {},
                  ),
                  TextButton(
                    child: Text("Produit par catégorie"),
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.greenAccent)),
                    onPressed: () {},
                  ),
                  TextButton(
                    child: Text("'A à Z'"),
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.greenAccent)),
                    onPressed: () {},
                  ),
                ],
              )
            ],
          ),
        ),
        Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Card(
                      child: Container(
                        height: 250,
                        width: 180,
                        child: Column(children: <Widget>[
                          Container(
                            height: 150,
                            width: 160,
                            decoration: BoxDecoration(
                              //color: Colors.blueGrey,
                              image: DecorationImage(
                                  image: AssetImage("/images/produit1.png")),
                            ),
                          ),
                          Text(
                            " 15 €",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          Text(
                            "Sample",
                            style: TextStyle(fontSize: 17),
                          )
                        ]),
                      ),
                    ),
                    Card(
                      child: Container(
                        height: 250,
                        width: 180,
                        child: Column(children: <Widget>[
                          Container(
                            height: 150,
                            width: 160,
                            decoration: BoxDecoration(
                              //color: Colors.blueGrey,
                              image: DecorationImage(
                                  image: AssetImage("/images/produit2.png")),
                            ),
                          ),
                          Text(
                            " 15,75 €",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          Text("sample")
                        ]),
                      ),
                    ),
                    Card(
                      child: Container(
                        height: 250,
                        width: 180,
                        child: Column(children: <Widget>[
                          Container(
                            height: 150,
                            width: 160,
                            decoration: BoxDecoration(
                              //color: Colors.blueGrey,
                              image: DecorationImage(
                                  image: AssetImage("/images/produit3.png")),
                            ),
                          ),
                          Text(
                            " 15,75 €",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          Text("sample")
                        ]),
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        )
      ]),
    ));
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}
