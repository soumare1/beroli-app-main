import 'package:flutter/material.dart';

class Profil_utilisateur extends StatelessWidget
    implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    const Color vertF = Color(0xFF146356);
    const Color blanc = Color(0xFFFFFFFF);
    const Color rouge = Color(0xFF722727);
    var isSelected = false;
    return Scaffold(
        body: Stack(children: [
      Positioned(
        top: 40,
        // position en Haut du widged " Positioned "
        left: 40,
        // position à Gauche du widged " Positioned "
        width: 150.0,
        // Largeur du widged " Positioned "
        height: 50.0,
        // Hauteur du widged " Positioned "
        child: MaterialButton(
          //initialisation du contenu du widget
          color: vertF,
          textColor: Colors.white,
          onPressed: () async {},
          child: const Text("Mes commandes",
              style: TextStyle(
                  fontSize: 20)), //ici je change la tail du text dans style
        ),
      ),
      Positioned(
        top: 40,
        right: 40,
        width: 150.0,
        height: 50.0,
        child: MaterialButton(
          color: vertF,
          textColor: Colors.white,
          onPressed: () async {},
          child: const Text("Mes informations", style: TextStyle(fontSize: 20)),
        ),
      ),
      Positioned(
        top: 110,
        left: 60,
        width: 300.0,
        height: 50.0,
        child: MaterialButton(
          color: rouge,
          textColor: Colors.white,
          onPressed: () async {},
          child: const Text("Mes informations", style: TextStyle(fontSize: 20)),
        ),
      ),
      Positioned(
        top: 180,
        left: 70,
        width: 300.0,
        height: 50.0,
        child: Icon(
          Icons.account_circle,
          //color: Colors.pink,
          size: 200.0,
        ),
      ),
    ]));
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}
