import 'package:flutter/material.dart';
import 'package:flutter_application_1/vues/vues_pages/panier_utilisateur.dart';
import 'package:flutter_application_1/vues/vues_pages/profil_utilisateur.dart';

import 'Home.dart';
import 'header.dart';

class Application_vues extends StatefulWidget {
  const Application_vues({Key? key}) : super(key: key);

  @override
  _BasicBottomNavBarState createState() => _BasicBottomNavBarState();
}

class _BasicBottomNavBarState extends State<Application_vues> {
  int _selectedIndex = 0;

  final List _listeDesPages = [
    Menu(),
    Profil_utilisateur(),
    Panier_utilisateur(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    const Color vertF = const Color(0xFF146356);
    const Color blanc = const Color(0xFFFFFFFF);
    return Scaffold(
      appBar: Header(),
      body: Center(
        child: _listeDesPages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'home',
            backgroundColor: vertF,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'account',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'panier',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'settings',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.chat),
          //   label: 'Chats',
          // ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        selectedItemColor: blanc,
      ),
    );
  }
}
