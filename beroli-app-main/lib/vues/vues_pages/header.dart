import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Header extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(50);

  @override
  Widget build(BuildContext context) {
    const Color vertF = const Color(0xFF146356);
    const Color blanc = const Color(0xFFFFFFFF);
    return AppBar(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/Beroli_logo.jpg',
            height: 50,
            width: 100,
            fit: BoxFit.contain,
          ),
        ],
      ),
      leading: const IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: blanc,
          size: 25,
        ),
        onPressed: null,
      ),
      
    );
  }
}
