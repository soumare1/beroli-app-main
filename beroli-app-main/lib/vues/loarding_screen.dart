import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

import 'home_screen.dart';

class loarding_screen extends StatefulWidget {
  loarding_screen({Key? key}) : super(key: key);

  @override
  State<loarding_screen> createState() => _loarding_screenState();
}

class _loarding_screenState extends State<loarding_screen> {
  @override
  void initState() {
    super.initState();
   // _navigatohome();
  }

  _navigatohome() async {
    await Future.delayed(Duration(milliseconds: 1500), () {});
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) =>HomeScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.green, Colors.white])),
      child: Scaffold(
          // By defaut, Scaffold background is white
          // Set its value to transparent
          backgroundColor: Colors.transparent,
          body: Center(
            child: Container(
              //borderRadius: BorderRadius.circular(20),
              //child: LoadingBouncingGrid.square(),
              child: Image.asset("assets/images/Beroli_logo.jpg"),
              width: 200,
              height: 200,
              color: Colors.white,
            ),
          )),
    );
  }
}
