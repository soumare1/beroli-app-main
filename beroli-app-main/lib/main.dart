//material

import 'package:flutter/material.dart';
//firebase
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_application_1/vues/vues_pages/Arborescence.dart';
import 'firebase_options.dart';
//animation
import 'package:animated_splash_screen/animated_splash_screen.dart';
//vues
import 'package:flutter_application_1/vues/auth_vues/registration_screen.dart';
import 'package:flutter_application_1/vues/home_screen.dart';
import 'package:flutter_application_1/vues/loarding_screen.dart';
import 'package:flutter_application_1/vues/auth_vues/login_screen.dart';
import 'utils/createMaterialColor.dart';
//token
//SPsOCNkysOdoC8kW7NdjyHtxXKB2

final createMaterialColor = CreateMaterialColor();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //final FirebaseAuth _auth = FirebaseAuth.instance;
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Beroli Accueil',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch:
              createMaterialColor.createMaterialColor(Color(0xFF146356)),
        ),

        //router
        /* initialRoute: '/',
        routes: {
          // When navigating to the "/" route, build the FirstScreen widget.
          '/signin': (context) => LoginScreen(),
          // When navigating to the "/second" route, build the SecondScreen widget.
          '/signup': (context) => RegistartionScreen(),
        },*/
        home: Application_vues());
    /* 
        AnimatedSplashScreen(
            duration: 1000,
            splash: Image.asset("assets/images/Beroli_logo.jpg"),
            nextScreen: HomeScreen(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor:
                createMaterialColor.createMaterialColor(Color(0xFF146356))));
  }*/
  }
}
